import { Get, JsonController } from "routing-controllers";
import { MazeCell } from "../models/MazeCell";
import { MazeGame } from "../models/MazeGame";
import { MazeResolution } from "../models/MazeResolution";
import { MazeService } from "../services/MazeService";

@JsonController('/maze')
export class MazeController {
    constructor(
        private mazeService: MazeService
    ){
        this.mazeService = new MazeService();
    }

    @Get("/resolve")
    public resolveMaze(): MazeResolution {
        var mazeSequence: string = "ABAAAAAAAAAA"
        + "ACADDEACCCDA"
        + "ACCDAEADADAA"
        + "AAAAAEDDADEA"
        + "ACCDDDAAAAEA"
        + "ACAAAAADDDEA"
        + "ADDDEEACAAAA"
        + "AAAEAEACCDDA"
        + "ADEEADAAAAAA"
        + "AADAADACDDAA"
        + "ADDDADCCADEB"
        + "AAAAAAAAAAAA";

        var maze: MazeCell[] = [];

        for (let index = 0; index < mazeSequence.length; index++) {
            var x = index % 12;
            var y = Math.floor(index / 12);
            maze.push(new MazeCell(x, y, mazeSequence[index]));
        }

        var sequence: string = "CCCDDDEEEDDD";
        var initialPosition: MazeCell = new MazeCell(1, 0, "B");
        var finalPosition: MazeCell = new MazeCell(11, 10, "B");
        var game: MazeGame = new MazeGame(maze, sequence, initialPosition, finalPosition);
        var resolution = this.mazeService.resolve(game);
        return resolution;
    }
}
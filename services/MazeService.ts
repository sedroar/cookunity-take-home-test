import { MazeGame } from "../models/MazeGame";
import { MazeResolution } from "../models/MazeResolution";
import { MazeStep } from "../models/MazeStep";
import { MazeCell } from "../models/MazeCell";

export class MazeService {

    public resolve(game: MazeGame): MazeResolution {
        const valuesMap = this.groupMazeCells(game.maze);

        const path: MazeStep[] = [];

        const resolution = new MazeResolution(
            this.findPath(new MazeStep(game.initialPosition), 0, game, valuesMap, MazeStep.NullStep, path)
            .map(step => step.cell));

        return resolution;
    }

    private groupMazeCells(mazeCells: MazeCell[]): Map<string, MazeCell[]> {
        //return _.groupBy(mazeCells, mazeCell => mazeCell.value);
        return mazeCells.reduce(function (r: Map<string, MazeCell[]>, a: MazeCell) {
            const cells = r.get(a.value) || [];
            cells.push(a);
            r.set(a.value, cells);
            return r;
        }, new Map<string, MazeCell[]>());
    }

    private findPath(step: MazeStep, index: number,
        mazeGame: MazeGame, valuesMap: Map<string, MazeCell[]>,
        checkPoint: MazeStep, path: MazeStep[]): MazeStep[] {

        let stepPath = path;

        stepPath.push(step);

        if (this.distanceIsOneStep(mazeGame.finalPosition, step)) {
            stepPath.push(new MazeStep(mazeGame.finalPosition));
        } else {
            const nextSteps = this.findNextSteps(step,
                path.length > 1 ? path[path.length - 2] : MazeStep.NullStep,
                mazeGame.sequence[index % mazeGame.sequence.length],
                valuesMap);

            const pathIndex = index + 1;

            if (nextSteps.length == 0) { // there are no next possible next steps from current cell
                stepPath.splice(0);
            } else if (nextSteps.length == 1) {
                // Add step to path
                stepPath = this.findPath(nextSteps[0], pathIndex, mazeGame, valuesMap, checkPoint, stepPath);
            } else {
                if (!nextSteps.some((nextStep) => { // there are more than one possible next steps from current cell
                    let pathFromNextStep: MazeStep[] = [];
                    pathFromNextStep = this.findPath(nextStep, pathIndex, mazeGame, valuesMap, step, pathFromNextStep);
                    if (pathFromNextStep.length > 0) {
                        stepPath = stepPath.concat(pathFromNextStep);
                        return true;
                    }
                    return false;
                })) {
                    stepPath.splice(0);
                }
            }
        }

        return stepPath;
    }

    private findNextSteps(step: MazeStep, previousStep: MazeStep, value: string, valuesMap: Map<string, MazeCell[]>): MazeStep[] {
        return (valuesMap.get(value) || [])
            .filter(cell => this.cellIsNotPreviousCell(previousStep, cell)
                && this.distanceIsOneStep(cell, step))
            .map(cell => new MazeStep(cell));
    }

    private cellIsNotPreviousCell(previousStep: MazeStep, cell: MazeCell) {
        return MazeStep.NullStep.cell.equals(previousStep.cell) ||
            ((!MazeStep.NullStep.cell.equals(previousStep.cell)) && !previousStep.cell.equals(cell));
    }

    private distanceIsOneStep(cell: MazeCell, step: MazeStep): unknown {
        return (Math.abs(cell.x - step.cell.x) === 1
            && Math.abs(cell.y - step.cell.y) === 0)
            || (Math.abs(cell.y - step.cell.y) === 1
                && Math.abs(cell.x - step.cell.x) === 0);
    }
}
import { expect } from "chai";
import path from "path/posix";
import { MazeCell } from "../models/MazeCell";
import { MazeGame } from "../models/MazeGame";
import { MazeService } from "../services/MazeService";

describe('Maze Service tests', () => { // the tests container
    it('checking success maze resolution', () => { // the single test
        var mazeSequence: string = "ABAAAAAAAAAA"
        + "ACADDEACCCDA"
        + "ACCDAEADADAA"
        + "AAAAAEDDADEA"
        + "ACCDDDAAAAEA"
        + "ACAAAAADDDEA"
        + "ADDDEEACAAAA"
        + "AAAEAEACCDDA"
        + "ADEEADAAAAAA"
        + "AADAADACDDAA"
        + "ADDDADCCADEB"
        + "AAAAAAAAAAAA";

        var maze: MazeCell[] = [];

        for (let index = 0; index < mazeSequence.length; index++) {
            var x = index % 12;
            var y = Math.floor(index / 12);
            maze.push(new MazeCell(x, y, mazeSequence[index]));
        }

        var sequence: string = "CCCDDDEEEDDD";
        var initialPosition: MazeCell = new MazeCell(1, 0, "B");
        var finalPosition: MazeCell = new MazeCell(11, 10, "B");
        var game: MazeGame = new MazeGame(maze, sequence, initialPosition, finalPosition);
        const mazeService = new MazeService();
        
        const resolution = mazeService.resolve(game);

        expect(resolution.path.length).greaterThan(0);
        expect(resolution.path[resolution.path.length -1 ].value).eq('B');
    }),
    it('checking no path found', () => { // the single test
        var mazeSequence: string = "ABAAAAAAAAAA"
        + "AXADDEACCCDA"
        + "ACCDAEADADAA"
        + "AAAAAEDDADEA"
        + "ACCDDDAAAAEA"
        + "ACAAAAADDDEA"
        + "ADDDEEACAAAA"
        + "AAAEAEACCDDA"
        + "ADEEADAAAAAA"
        + "AADAADACDZAA"
        + "ADDDADCCADZB"
        + "AAAAAAAAAAAA";

        var maze: MazeCell[] = [];

        for (let index = 0; index < mazeSequence.length; index++) {
            var x = index % 12;
            var y = Math.floor(index / 12);
            maze.push(new MazeCell(x, y, mazeSequence[index]));
        }

        var sequence: string = "CCCDDDEEEDDD";
        var initialPosition: MazeCell = new MazeCell(1, 0, "B");
        var finalPosition: MazeCell = new MazeCell(11, 10, "B");
        var game: MazeGame = new MazeGame(maze, sequence, initialPosition, finalPosition);
        const mazeService = new MazeService();
        
        const resolution = mazeService.resolve(game);

        expect(resolution.path.length).eq(0);
    })
});
# Sergio Rossetti's Cook Unity Take Home Test

The approach I took was the following:
1. Load the maze cells in a list
2. Group the cells of the maze by value and build a map using the values as keys and an array of cells as values
3. Recursively traverse through the cells following the sequence
4. Based on the next value of the sequence, I look for the possible possible next steps in the map.

## How to run
1. npm install  
2. npm run build  
3. npm start  

## Resolve maze
curl --location --request GET 'http://localhost:3000/maze/resolve'

## How to test
1. npm test


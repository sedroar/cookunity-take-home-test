// this shim is required
import { createExpressServer } from 'routing-controllers';
import { MazeController } from './controllers/MazeController';

// creates express app, registers all controller routes and returns you express app instance
const app = createExpressServer({
    controllers: [MazeController], // we specify controllers we want to use
  });
  
  // run express application on port 3000
  app.listen(3000, () => {
      console.log("The application is running on port 3000");
  });
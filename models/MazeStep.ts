import { MazeCell } from "./MazeCell";

export class MazeStep {

    public static NullStep = new MazeStep(new MazeCell(-1, -1, ''));

    constructor(
        public cell: MazeCell
    ) { }
}
import { MazeStep } from "./MazeStep";
import { MazeCell } from "./MazeCell";

// This class defines a game, which consists of a maze and the sequence of letters to follow
export class MazeGame {
    constructor(public maze: MazeCell[],
        public sequence: string,
        public initialPosition: MazeCell,
        public finalPosition: MazeCell) { }
}
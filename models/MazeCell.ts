// Represents a position in the maze
export class MazeCell {
    constructor (
        public x: number,
        public y: number,
        public value: string
    ) { }

    public equals(otherCell: MazeCell): boolean {
        return this.x === otherCell.x
            && this.y === otherCell.y
            && this.value === otherCell.value;
    }
}
import { MazeCell } from "./MazeCell";

export class MazeResolution {
    constructor(public path: MazeCell[]) {
    }
}